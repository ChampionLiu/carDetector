TARGET = detect

SRCS := $(wildcard ./*.cpp src/*.cpp) 

OBJECTS := $(patsubst %cpp,%o,$(SRCS))

CFLAGS = -g -Wall -I/usr/local/include -I/usr/local/include/opencv2 -Iinc -std=c++11 -DCALCULATE_MODE=1 -DPLATFORM=1 

LDFLAGS = -Wl,-rpath,./ -lrt -L./ -L/usr/local/lib/ -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_objdetect

CXX = g++

$(TARGET) : $(OBJECTS)
	$(CXX) -o $(TARGET) $(OBJECTS) $(LDFLAGS)

%.o:%.cpp  
	$(CXX) $(CFLAGS) -c $< -o $@ 

clean:
	rm src/*.o 
