#ifndef __CARDETECT_H
#define __CARDETECT_H

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>
#include <string>

using namespace cv;
using namespace std;

class CarDetect
{
public:
	CarDetect();

	void create(string _file_name = "./dat/car.xml");
	bool detect(cv::Mat InputFrame, std::vector<cv::Rect>& cars);
	void draw(cv::Mat InputPic, cv::Mat& OutputPic, cv::Rect rect);
	
private:
	cv::CascadeClassifier detectors;
};

#endif
