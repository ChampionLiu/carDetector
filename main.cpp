#include "carDetect.h"

#define WIN_NAME "Car Detect"

using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	if(argc == 1)
	{
		cout << "no detect video input. please input cmd:" << endl;
		cout << "detect [video name] <enter>" << endl;
		return -1;
	}

	VideoCapture cap(argv[1]);
	if(!cap.isOpened() )
	{
		cout << "error in openning the video." << endl;
		return -2;
	}

	Mat frame, img;
	vector<Rect> cars;
	CarDetect finder;

	finder.create();
	namedWindow(WIN_NAME);

	while(true)
	{
		cap >> frame;
		if(!frame.empty() )
		{
			//TODO:
			if(finder.detect(frame, cars) )
			{
				for(size_t i = 0; i < cars.size(); i++)
					finder.draw(frame, img, cars[i]);
			}
			else
			{
				img = frame;
			}

			imshow(WIN_NAME, img);

			double rate = cap.get(CV_CAP_PROP_FPS);
			cout << rate << endl;
		}
		else
		{
			cout << "video ending." << endl;
			break;
		}

		char cmd = waitKey(10);
		if(cmd == 'c')
			break;
	}

	return 0;
}
