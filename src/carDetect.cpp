#include "carDetect.h"

using namespace cv;
using namespace std;

CarDetect::CarDetect()
{
	cout << "car detector init." << endl;
	cout << "please use \"load()\" to load xml files" << endl;
}

void CarDetect::create(string _file_name)
{
	detectors.load(_file_name);
}

bool CarDetect::detect(cv::Mat InputFrame, std::vector<cv::Rect>& cars)
{
	cv::Mat img;
	cvtColor(InputFrame, img, CV_BGR2GRAY);
	equalizeHist(img, img);

	detectors.detectMultiScale(img, cars, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(200,100) );
	
	if(cars.size() > 0)
		return true;
	else
		return false;
}

void CarDetect::draw(cv::Mat InputPic, cv::Mat& OutputPic, cv::Rect rect)
{
	OutputPic = InputPic.clone();
	rectangle(OutputPic, rect, Scalar(0,255,0) ,2, 8, 0);
}
